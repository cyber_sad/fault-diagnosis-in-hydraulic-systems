### Download the data

Use dvc (https://dvc.org) after cloning the project:

```
dvc pull
```

### Dataset definition file

```
fp, cid, name
/home/vlpr/efd_project/data/simulation/07.11.19_nominal.npy, 0
/home/vlpr/efd_project/data/simulation/07.11.19_pga_leak.npy, 1
/home/vlpr/efd_project/data/simulation/09.12.20_fluid_leak_0.600mm.npy, 2
/home/vlpr/efd_project/data/simulation/10.12.20_valve_set_error_30.00bar.npy, 3
/home/vlpr/efd_project/data/simulation/10.12.20_valve_set_error_33.00bar.npy, 3
```
### WindowDataset class usage

```
aa = WindowDataset('test.csv').get_generator(
    get_item=lambda cid, w: (cid, w[:,:2]), # w[timesteps, channels]
    get_types=lambda: (tf.int8, tf.float32),
    get_shape=lambda: (tf.TensorShape([]), tf.TensorShape([1, 2])))
```
