from itertools import cycle, islice, repeat
import tensorflow as tf
import pandas as pd
import numpy as np
import os

def roundrobin(*iterables):
    "roundrobin('ABC', 'D', 'EF') --> A D E B F C"
    # Recipe credited to George Sakkis
    num_active = len(iterables)
    nexts = cycle(iter(it).__next__ for it in iterables)
    while num_active:
        try:
            for next in nexts:
                yield next()
        except StopIteration:
            # Remove the iterator we just exhausted from the cycle.
            num_active -= 1
            nexts = cycle(islice(nexts, num_active))

class WindowDataset:
    def __init__(self, dataset_description_path, window_size=1, initial_shuffle=False, every=1, portion=1.0, active_channels=None, verbose=False):
        """
        test.csv

        fp, cid, name, 
        simulation/07.11.19_nominal.npy, 0
        simulation/07.11.19_pga_leak.npy, 1
        simulation/09.12.20_fluid_leak_0.600mm.npy, 2
        simulation/10.12.20_valve_set_error_30.00bar.npy, 3
        simulation/10.12.20_valve_set_error_33.00bar.npy, 3


        aa = WindowDataset('test.csv').get_generator(
        get_item=lambda cid, w: (cid, w[:2,:].T), # w[timesteps, channels]
        get_types=lambda: (tf.int8, tf.float32),
        get_shape=lambda: (tf.TensorShape([]), tf.TensorShape([1, 2])))
        
        Source should be stored channel-wise.
        """
        assert window_size > 0
        self.window_size = window_size
        
        fps, timeseries, class_names, class_labels = self.__load_data(dataset_description_path)
        
        if verbose:
            for x,y in zip(fps, timeseries):
                print(x, y.shape)
                
        self.class_names = {k:v for k,v in zip(class_labels, class_names)}
        
        self.timeseries_file_paths = fps
        self.timeseries = timeseries
        self.timeseries_class_names = class_names
        self.timeseries_class_labels = class_labels

        self.number_of_classes = len(self.class_names.values())
        self.number_of_source_files = len(self.timeseries_file_paths)
        self.number_of_windows = 0
        
        self.active_channels = 9223372036854775807 if active_channels is None else active_channels
        
        self.wids = []
        for idx, ts in enumerate(self.timeseries):
            timesteps = ts.shape[1]
            assert timesteps > self.window_size
            wids = np.arange(timesteps-self.window_size+1)
                       
            wids = wids[::every]
            
            if initial_shuffle:
                np.random.shuffle(wids)
            
            wids = wids[:int(portion*len(wids))]
                        
            self.wids.append(wids)
            self.number_of_windows += len(wids)

        min_number_of_channels = np.min([w.shape[0] for w in self.wids])
        if not active_channels is None:
            assert active_channels <= min_number_of_channels
        self.active_channels = min(min_number_of_channels, active_channels)
        
        for i in range(self.number_of_source_files):
            self.timeseries[i] = self.timeseries[i][:self.active_channels,:]

    def __load_data(self, dataset_description_path):
        dataset_description_dir = os.path.split(dataset_description_path)[0] + '/'
        dsd = pd.read_csv(dataset_description_path, skipinitialspace=True)
        fps = []
        tss = []
        class_names = []
        class_labels = []
        for idx, (fp, cid, name) in dsd.iterrows():
            fp = fp if os.path.isabs(fp) else os.path.join(dataset_description_dir,fp)
            fps.append(fp)
            tss.append(np.load(fp))
            class_names.append(name)
            class_labels.append(cid)
        assert len(tss) == len(class_names) == len(class_labels)
        return fps, tss, class_names, class_labels
    
    def __repr__(self):
        return str(self.__dict__)
    
    def get_generator(self, get_item, get_types, get_shape, train_test_split_ratio = 0.0, train=False, shuffle_windows=False, kidx=0, return_true_labels=False):
        """Window shape: [channels, timesteps]"""

        wids = []
        for idx, x in enumerate(self.wids):
            x = x.copy()

            fold_size = int((1 - train_test_split_ratio) * len(x))
            np.roll(x, kidx*fold_size)

            split_idx = int(train_test_split_ratio*len(x))
            x = x[:split_idx] if train else x[split_idx:]

            assert (len(x) > 0), 'Small {} split for window size {} in {}.'.format('train' if train else 'test', self.window_size, self.timeseries_file_paths[idx])
            wids.append(x)        
        
        def gen():
            if shuffle_windows:
                for x in wids:
                    np.random.shuffle(x)

            file_wids_iter = []
            for file_idx, file_wids in enumerate(wids):
                file_wids_iter.append(zip(repeat(file_idx, len(file_wids)), file_wids))
                
            for file_idx, file_wid in roundrobin(*file_wids_iter):
                x = [self.timeseries_class_labels[file_idx], self.timeseries[file_idx][:,file_wid:file_wid+self.window_size]]
                yield get_item(*x)
        
        ds = tf.data.Dataset.from_generator(
             gen, 
             get_types(),
             get_shape())
        
        if return_true_labels:
            true_labels = np.array(list(roundrobin(*[repeat(cid, len(ts_wids)) for ts_wids, cid in zip(wids, self.timeseries_class_labels)])))
            return ds, true_labels
        else:
            return ds
    

    def __getitem__(self, pos):
        file_idx, timestep = pos
        return self.timeseries[file_idx][:,timestep:timestep+self.window_size]

    
class WindowDatasetStack:
    def __init__(self, *window_datasets):
        self.window_size = np.min([wd.window_size for wd in window_datasets])
        self.active_channels = np.min([wd.number_of_windows for wd in window_datasets])
        
        self.window_datasets = window_datasets
        self.class_names = dict(i for wd in window_datasets for i in wd.class_names.items())
        self.timeseries_file_paths = [i for wd in window_datasets for i in wd.timeseries_file_paths]
        self.timeseries = [i for wd in window_datasets for i in wd.timeseries]
        self.timeseries_class_names = [i for wd in window_datasets for i in wd.timeseries_class_names]
        self.timeseries_class_labels = [i for wd in window_datasets for i in wd.timeseries_class_labels]

        self.number_of_classes = np.sum([wd.number_of_classes for wd in window_datasets])
        self.number_of_source_files = np.sum([wd.number_of_source_files for wd in window_datasets])
        self.number_of_windows = np.sum([wd.number_of_windows for wd in window_datasets])
        
                
    def get_generator(self, get_item, get_types, get_shape, train_test_split_ratio = [0.0], train=False, shuffle_windows=False, kidx=0, return_true_labels=False):
        wids = []
        global_idx = 0
        for wds_idx, wds in enumerate(self.window_datasets):
            for idx, x in enumerate(wds.wids):
                x = x.copy()

                fold_size = int((1 - train_test_split_ratio[wds_idx]) * len(x))
                np.roll(x, kidx*fold_size)

                split_idx = int(train_test_split_ratio[wds_idx]*len(x))
                x = x[:split_idx] if train else x[split_idx:]

                assert (len(x) > 0), 'Small {} split for window size {} in {}.'.format('train' if train else 'test', self.window_size, self.timeseries_file_paths[global_idx])
                wids.append(x) 
                global_idx += 1
        
        def gen():
            if shuffle_windows:
                for x in wids:
                    np.random.shuffle(x)

            file_wids_iter = []
            for file_idx, file_wids in enumerate(wids):
                file_wids_iter.append(zip(repeat(file_idx, len(file_wids)), file_wids))
                
            for file_idx, file_wid in roundrobin(*file_wids_iter):
                x = [self.timeseries_class_labels[file_idx], self.timeseries[file_idx][:,file_wid:file_wid+self.window_size]]
                yield get_item(*x)
        
        ds = tf.data.Dataset.from_generator(
             gen, 
             get_types(),
             get_shape())
        
        if return_true_labels:
            true_labels = np.array(list(roundrobin(*[repeat(cid, len(ts_wids)) for ts_wids, cid in zip(wids, self.timeseries_class_labels)])))
            return ds, true_labels
        else:
            return ds