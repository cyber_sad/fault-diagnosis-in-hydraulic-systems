FROM tensorflow/tensorflow:2.4.1-gpu-jupyter
ENV SHELL=/bin/bash
EXPOSE 8888
WORKDIR /efd_nn
RUN pip install jupyterlab tensorflow_addons seaborn scikit-learn dvc[webdav]
CMD source /efd_nn/env.sh; jupyter lab --ip 0.0.0.0 
