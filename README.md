### Paper

Gareev, A.; Protsenko, V.; Stadnik, D.; Greshniakov, P.; Yuzifovich, Y.; Minaev, E.; Gimadiev, A.; Nikonorov, A. Improved Fault Diagnosis in Hydraulic Systems with Gated Convolutional Autoencoder and Partially Simulated Data. Sensors 2021, 21, 4410. https://doi.org/10.3390/s21134410

### Dependencies

```
python=3.8 
pandas 
numpy 
matplotlib 
seaborn 
scikit-learn 
scipy 
pyarrow 
jupyterlab
tensorflow=2.4.1
xlrd 
dvc[webdav]=1.11.16
```

### Initialize project environment
```
source ./env.sh
```

### Docker
```
docker build -t efd_nn .
docker run -it --rm --gpus all -u $(id -u):$(id -g) -v $(pwd):/efd_nn -e USER -w /efd_nn -p 8888:8888 efd_nn
```

### Dataset and trained models download
```
dvc pull
```

Also available at: [Dataset](https://storage-liav.ipsiras.ru/index.php/s/NS586B8FoRrtRE3), [Trained models](https://storage-liav.ipsiras.ru/index.php/s/8LZ4ozXd8AeD9g7)

## Steps to reproduce results with docker
###  Prerequisites
 - Linux OS
 - Nvidia GPU
 - Docker (https://docs.docker.com/engine/install/ubuntu/)
 - nvidia-docker (https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker)

1. Download code to any folder with >10GB free disk space 
```
git clone https://gitlab.com/protsenkovi/efd_nn/; cd efd_nn
```
2. Build the docker image 
```
docker build -t efd_nn .
```
3. Download dataset with the help of docker container (repeat in case download errors occured)
```
docker run -it --rm -u $(id -u):$(id -g) -v $(pwd):/efd_nn -e USER -w /efd_nn efd_nn bash -l -c "dvc pull"
```
4. Run containerized jupyter notebook server that is listeting on port 8888:
```
docker run -it --rm --gpus all -u $(id -u):$(id -g) -v $(pwd):/efd_nn -e USER -w /efd_nn -p 8888:8888 efd_nn
```
5. Go to the jupyter notebook web page and run all cells of the `.ipynb` notebook file in `notebook` directory which results should be reproduced.
